from flask import Flask, request, render_template, request, make_response, jsonify
from flask_sqlalchemy import SQLAlchemy;
from sqlalchemy_utils.functions import database_exists
import uuid

from werkzeug.utils import redirect

domain = "w3cstats.mcnabbapps.com"

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///w3cstats.db'
db = SQLAlchemy(app)

class UserCookie(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    cookieID = db.Column(db.String(21), unique = True, nullable = False)
    playerOne = db.Column(db.String(80), unique = False, nullable = True)
    playerTwo = db.Column(db.String(80), unique = False, nullable = True)    
    playerOneRace = db.Column(db.String(80), unique = False, nullable = True)
    playerTwoRace = db.Column(db.String(80), unique = False, nullable = True)   
    map = db.Column(db.String(80), unique = False, nullable = True)   
    season = db.Column(db.String(80), unique = False, nullable = True)   
    needsUpdate = db.Column(db.String(80), unique = False, nullable = True, default="false")

    def __repr__(self):
        return self.cookieID

def create_user(res):
    cookieID = str(uuid.uuid1().int>>64)
    res.set_cookie('cookieID', value=cookieID, max_age=None)
    insert_User(cookieID)
    return res, cookieID

@app.route('/')
@app.route('/index')
def my_app():
    create_Database()
    res = make_response()
    cookieID = request.cookies.get('cookieID')
    if cookieID == None:
        res, cookieID = create_user(res)
    player = get_Last_Known_Players(cookieID)
    if (player == None):
        res.delete_cookie("cookieID")
        res.set_data(render_template('redirect.html'))
        return res
    player_one = player[0]
    player_two = player[1]
    player_one_race = player[2]
    player_two_race = player[3]
    map = player[4]
    season = player[5]
    res.set_data(render_template(('index.html'), username=cookieID, playerOne=player_one, playerTwo=player_two, domain=domain, playerOneRace=player_one_race, playerTwoRace=player_two_race, map=map, season=season))
    return res

@app.route('/a/<string:id>')
def admin_override(id):
    cookieID = id
    players = get_Last_Known_Players(cookieID)
    player_one = players[0]
    player_two = players[1]
    player_one_race = players[2]
    player_two_race = players[3]
    map = players[4]
    season = players[5]
    return render_template(('index.html'), username=cookieID, playerOne=player_one, playerTwo=player_two, domain=domain, playerOneRace=player_one_race, playerTwoRace=player_two_race, map=map, season=season)

@app.route('/a', methods=['POST'])
def admin_override_post():
    id = request.form['adminID']; 
    set_Last_Known_Players(id, request.form['playerOne'], request.form['playerTwo'], request.form['playerOneRace'], request.form['playerTwoRace'], request.form['map'], request.form['season'])
    return redirect('/a/' + id)
    
@app.route('/', methods=['POST'])
@app.route('/index', methods=['POST'])
def submit_Form():
    #after information is submited, give links to various overlays
    userCookie = request.cookies.get('cookieID')
    set_Last_Known_Players(userCookie, request.form['playerOne'], request.form['playerTwo'], request.form['playerOneRace'], request.form['playerTwoRace'], request.form['map'], request.form['season'])
    return redirect('/')

@app.route('/refreshdata/<string:id>', methods=['POST', 'GET'])
def refresh_Data(id):
    if (request.method == 'GET'):
        return get_Needs_Update(id)

@app.route('/1v1/<string:id>')
def one_v_one(id):
    #here we will return a template showing the 1v1 stats of players vs each other
    players = get_Last_Known_Players(id)
    player_one = players[0]
    player_two = players[1]
    player_one_race = players[2]
    player_two_race = players[3]
    map = players[4]
    season = players[5]
    return render_template(('1v1.html'), username=id, playerOne=player_one, playerTwo=player_two, domain=domain, playerOneRace=player_one_race, playerTwoRace=player_two_race, map=map, season=season)

@app.route('/u/<string:id>')
def user(id):
    #here we will return a template showing the active game of player one
    #might want to update info every few seconds if W3C API allows
    return id

def set_Last_Known_Players(id, playerOne, playerTwo, playerOneRace, playerTwoRace, map, season):
    user = UserCookie.query.filter_by(cookieID=id).first()
    user.playerOne = playerOne
    user.playerTwo = playerTwo
    user.playerOneRace = playerOneRace
    user.playerTwoRace = playerTwoRace
    user.map = map
    user.season = season
    user.needsUpdate = "true"
    db.session.commit()

@app.route('/u/a/<string:id>')
def get_JSON_User_Info(id):
    user = UserCookie.query.filter_by(cookieID=id).first()
    playerOne = user.playerOne
    playerTwo = user.playerTwo
    playerOneRace = user.playerOneRace
    playerTwoRace = user.playerTwoRace
    map = user.map
    season = user.season
    return '["' + playerOne + '", "' + playerTwo + '", "' + playerOneRace  + '", "' + playerTwoRace + '", "' +  map  + '", "' + season + '"]'
     
def get_Needs_Update(id):
    user = UserCookie.query.filter_by(cookieID=id).first()
    if (user.needsUpdate == "true"):
        user.needsUpdate = "false"
        db.session.commit()
        return "true"
    else: return "false"

def get_Last_Known_Players(id):
    user = UserCookie.query.filter_by(cookieID=id).first()
    if (user == None):
        return None
    playerOne = user.playerOne
    playerTwo = user.playerTwo
    playerOneRace = user.playerOneRace
    playerTwoRace = user.playerTwoRace
    map = user.map
    season = user.season
    return [playerOne, playerTwo, playerOneRace, playerTwoRace, map, season]

def insert_User(id):
    new_user_cookie = UserCookie(cookieID=id)
    db.session.add(new_user_cookie)
    db.session.commit()

def create_Database():
    if database_exists(app.config["SQLALCHEMY_DATABASE_URI"]) != True:
        db.create_all()