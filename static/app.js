var GameMode = 1; //1v1
var Gateway = 20; //Europe
var Season = 9;
var Races = {"0":"RD", "1":"HU", "2":"OC", "4":"NE", "8":"UD"}
var Domain = "http://w3cstats.mcnabbapps.com"

var Maps = {"EchoIsles": "EI",
            "EchoIslesv2_2": "EI2",
            "TerenasStandLV": "TS", 
            "ConcealedHill": "CH", 
            "NorthernIsles": "NIS", 
            "Amazonia": "AZ", 
            "LastRefuge": "LR", 
            "LastRefugev1_4": "LR",
            "LastRefugev1_5": "LR",
            "LostTemple": "LT",
            "tidehunters": "TH", 
            "Tidehuntersv1_2": "TH",
            "AutumnLeavesv2-0": "AL",
            "SecretValleyv2_0a": "SV",
            "ShatteredExilev2-07": "SE",
            "ShallowGravev1_4": "SG",
            "TurtleRock": "TR",
            "Springtimev1_1": "ST",
            "Springtimev1_2": "ST",
            "RustyCreekv1_1": "RC",
            "RustyCreekv1_2": "RC",
            "tbd": "TBD"}

var MapNames = {"EchoIsles": "Echo Isles", 
                "EchoIslesv2_2": "Echo Isles 2.2",
                "TerenasStandLV": "Terenas Stand", 
                "ConcealedHill": "Concealed Hill", 
                "NorthernIsles": "Northern Isles", 
                "Amazonia": "Amazonia", 
                "LastRefuge": "Last Refuge", 
                "LastRefugev1_4": "Last Refuge 1.4",
                "LastRefugev1_5": "Last Refuge 1.5",
                "LostTemple": "Lost Temple",
                "tidehunters": "Tidehunters", 
                "Tidehuntersv1_2": "Tide Hunters 1.2",
                "AutumnLeavesv2-0": "Autumn Leaves",
                "SecretValleyv2_0a": "Secret Valley",
                "ShatteredExilev2-07": "Shattered Exile",
                "ShallowGravev1_4": "Shallow Grove",
                "TurtleRock": "Turtle Rock",
                "Springtimev1_1": "Springtime",
                "Springtimev1_2": "Springtime 1.2",
                "RustyCreekv1_1": "Rusty Creek",
                "RustyCreekv1_2": "Rusty Creek 1.2",
                "tbd": "TBD"}

var Ranks = {
    0: "Grandmaster",
    1: "Master",
    2: "Adept",
    3: "Diamond",
    4: "Platinum",
    5: "Gold",
    6: "Silver",
    7: "Bronze",
    8: "Grass"
}

var raceBackgrounds = {
    "0": "rgba(0,0,0,0)",
    "1": "rgba(90, 175, 210, 0.65)",
    "2": "rgba(107, 0, 11, 0.65)",
    "4": "rgba(0, 122, 82, 0.65)",
    "8": "rgba(62, 0, 110, 0.65)"
}

var ApiLink = "https://statistic-service.w3champions.com"

// Could potentially extend this to win rate vs opponent on map
function WinRateVsOpponentCallback(response)
{
    matches = JSON.parse(response).matches
    var totalMatches = 0;
    var playerOneWins = -1;
    var playerTwoWins = -1;
    
    var playerOneBnetTag = document.getElementById("playerOne").value;
    var playerTwoBnetTag = document.getElementById("playerTwo").value;

    if (matches.length == 0)
    {
        document.getElementById('playerOneOpponentWinRate').innerHTML = " N/A";
        document.getElementById('playerTwoOpponentWinRate').innerHTML = " N/A";
        return;
    }

    matches.forEach(element => {
        totalMatches += 1;
        element.teams.forEach(element => {
            if (playerOneBnetTag == element.players[0].battleTag && element.won == true) {
                if (playerOneWins == -1)
                {
                    playerOneWins = 0;
                }
                playerOneWins += 1;
            } 
            if (playerTwoBnetTag == element.players[0].battleTag && element.won == true) {
                if (playerTwoWins == -1)
                {
                    playerTwoWins = 0;
                }
                playerTwoWins += 1;
            }
        });
    });

    playerOneWinRate = " " + (playerOneWins / totalMatches * 100).toFixed(2) + "%";
    playerTwoWinRate = " " + (playerTwoWins / totalMatches * 100).toFixed(2) + "%";

    document.getElementById('playerOneOpponentWinRate').innerHTML = playerOneWinRate;
    document.getElementById('playerTwoOpponentWinRate').innerHTML = playerTwoWinRate;
}

function LoadPlayerOneCallback(response)
{
    var data = JSON.parse(response);
    var raceNumber = document.getElementById('playerOneRace').value;
    var race = null;
    var raceNumber = document.getElementById("playerOneRace").value;

    data.forEach(element => {
        if (element.race == raceNumber && element.gameMode == GameMode)
        {
            race = element;
         }
    });

    if (race == null)
    {
        document.getElementById('playerOneWins').innerHTML = " 0";
        document.getElementById('playerOneLosses').innerHTML = " 0";
        document.getElementById('playerOneMMR').innerHTML = " N/A";
        document.getElementById('playerOneWinRate').innerHTML = " N/A";
        document.getElementById('playerOneRankLabel').innerHTML = "";
        return;
    }

    if (race.games >= 5)
    {
        document.getElementById('playerOneRankLabel').innerHTML = "<img id='rankImage' src='" + GetRankIcon(race.leagueOrder) + "'>";
    }

    var mmr = race.mmr;
    var winRate = race.winrate;
    document.getElementById('playerOneMMR').innerHTML = " " + mmr;
    document.getElementById('playerOneWinRate').innerHTML = " " + (winRate * 100).toFixed(2) + "%";

    wins = race.wins;
    losses = race.losses;
    document.getElementById('playerOneWins').innerHTML = " " + wins;
    document.getElementById('playerOneLosses').innerHTML = " " + losses;
}

function LoadPlayerTwoCallback(response)
{

    var data = JSON.parse(response);
    var raceNumber = document.getElementById('playerTwoRace').value;
    var race = null;

    elementRaces = "";

    data.forEach(element => {
        if (element.race == raceNumber && element.gameMode == GameMode)
        {
            elementRaces += element.race + " ,"
            race = element;
        } 
    });

    if (race == null)
    {
        document.getElementById('playerTwoWins').innerHTML = " 0";
        document.getElementById('playerTwoLosses').innerHTML = " 0";
        document.getElementById('playerTwoMMR').innerHTML = " N/A";
        document.getElementById('playerTwoWinRate').innerHTML = " N/A";
        document.getElementById('playerTwoRankLabel').innerHTML = "";

        //remove me
        document.getElementById('playerTwoWinRate').innerHTML = "elementRaces";
    }

    if (race.games >= 5)
    {
        document.getElementById('playerTwoRankLabel').innerHTML = "<img id='rankImage' src='" + GetRankIcon(race.leagueOrder) + "'>";
    }

    var mmr = race.mmr;
    var winRate = race.winrate;
    document.getElementById('playerTwoMMR').innerHTML = " " + mmr;
    document.getElementById('playerTwoWinRate').innerHTML = " " + (winRate * 100).toFixed(2) + "%";

    var wins = race.wins;
    var losses = race.losses;
    document.getElementById('playerTwoWins').innerHTML = " " + wins;
    document.getElementById('playerTwoLosses').innerHTML = " " + losses;
}

function LoadAndRefresh()
{
    LoadPlayerData();
    setInterval(Refresh, 1000)
}

function RefreshCallback(response)
{
    if (response == "true")
    {
        UpdateData();
    }
}

function Refresh()
{
    var url = Domain + "/refreshdata/" + document.getElementById("username").value
    httpGetAsync(url, RefreshCallback)
}

function Updatecallback(response)
{
    var list = JSON.parse(response)
    document.getElementById("playerOne").value = list[0]
    document.getElementById("playerTwo").value = list[1]
    document.getElementById("playerOneRace").value = list[2]
    document.getElementById("playerTwoRace").value = list[3]
    document.getElementById("map").value = list[4]
    document.getElementById("season").value = list[5]
    LoadPlayerData()
}

function UpdateData()
{
    var url =  Domain + "/u/a/" + document.getElementById("username").value
    httpGetAsync(url, Updatecallback)
}

function LoadPlayerData()
{
    playerOne = document.getElementById("playerOne").value;
    playerOneURLCompat = playerOne.split('#')[0] + "%23" + playerOne.split('#')[1];
    playerTwo = document.getElementById("playerTwo").value;
    playerTwoURLCompat = playerTwo.split('#')[0] + "%23" + playerTwo.split('#')[1];
    season = document.getElementById("season").value;

    var playerName = document.getElementById("playerOne").value;
    var raceNumber = document.getElementById("playerOneRace").value;
    document.getElementById('playerOneName').innerHTML = playerName.split("#")[0];
    document.getElementById('playerOneRaceLabel').innerHTML = "<img src='" + GetRaceImageURL(raceNumber) + "'>";
    document.getElementById('playerOneRaceLabel').style.backgroundColor = raceBackgrounds[raceNumber];

    var playerName = document.getElementById("playerTwo").value;
    var raceNumber = document.getElementById("playerTwoRace").value;
    document.getElementById('playerTwoName').innerHTML = playerName.split("#")[0];
    document.getElementById('playerTwoRaceLabel').innerHTML = "<img src='" + GetRaceImageURL(raceNumber) + "'>";
    document.getElementById('playerTwoRaceLabel').style.backgroundColor = raceBackgrounds[raceNumber];

    //get player one mmr/name/winrate
    var url = "https://statistic-service.w3champions.com/api/players/" + playerOneURLCompat + "/game-mode-stats?gateWay=20&season=" + season
    httpGetAsync(url, LoadPlayerOneCallback)

    //get player two mmr/name/winrate
    var url = "https://statistic-service.w3champions.com/api/players/" + playerTwoURLCompat + "/game-mode-stats?gateWay=20&season=" + season
    httpGetAsync(url, LoadPlayerTwoCallback)

    //get win rates for both players against each other
    var url = "https://statistic-service.w3champions.com/api/matches/search?playerId=" + playerTwoURLCompat + "&season=" + season + "&opponentId=" + playerOneURLCompat + "&gameMode=1&gateWay=20&pageSize=100"
    httpGetAsync(url, WinRateVsOpponentCallback)

    //get win rate vs race on map for player one
    var url = "https://statistic-service.w3champions.com/api/player-stats/" + playerOneURLCompat + "/race-on-map-versus-race?season=" + season
    httpGetAsync(url, WinRateOnMapVSRaceCallback)

    //get win rate vs race on map for player two
    var url = "https://statistic-service.w3champions.com/api/player-stats/" + playerTwoURLCompat + "/race-on-map-versus-race?season=" + season
    httpGetAsync(url, WinRateOnMapVSRaceCallback, false)
}

// function GetCurrentGameCallback(response)
// {
//     var map;
//     var playerOne;
//     var playerOneRace;
//     var playerTwo;
//     var playerTwoRace;

//     document.getElementById("playerOne").value = playerOne;
//     document.getElementById("playerOneRace").value = playerOneRace;
//     document.getElementById("playerTwo").value = playerTwo;
//     document.getElementById("playerTwoRace").value = playerTwoRace;
//     document.getElementById("map").value = map;

//     LoadPlayerData()
// }

//function GetCurrentGame(player)
//{
    //template link for player's ongoing match
    //https://localhost:5001/api/matches/ongoing/seanmcnabb%231233
    //var url = "https://statistic-service.w3champions.com/api/matches/ongoing/" + player;
//}

// function Submit()
// {
//     playerOne = document.getElementById("playerOne").value;
//     playerTwo = document.getElementById("playerTwo").value;
// }

function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function httpGetAsync(theUrl, callback, isPlayerOne=true)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText, isPlayerOne);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}

//There is an error here being hacked around if a player hasn't played on the map with their race
//TODO Fix error
function WinRateOnMapVSRaceCallback(response, isPlayerOne=true)
{
    var data = JSON.parse(response);

    var playerOneRaceNumber;
    var playerTwoRaceNumber;

    if (isPlayerOne)
    {
        playerOneRaceNumber = document.getElementById('playerOneRace').value;
        playerTwoRaceNumber = document.getElementById('playerTwoRace').value;
    } else {
        playerOneRaceNumber = document.getElementById('playerTwoRace').value;
        playerTwoRaceNumber = document.getElementById('playerOneRace').value;
    }

    var map = document.getElementById('map').value;
    document.getElementById('mapLabel').innerHTML = MapNames[map];

    if (map != "tbd")
    {
        document.getElementById('mapImage').innerHTML = "<img src='" + GetMapImageLink(map) + "'>"
    }

    var winRateVsRace = null;
    var winRateVsRaceOnMap = null;

    data.raceWinsOnMapByPatch.All.forEach(element => {
        if (element.race == playerOneRaceNumber)
        {
            element.winLossesOnMap.forEach(element => {
                if (element.map == "Overall")
                {
                    element.winLosses.forEach(element => {
                        if (playerTwoRaceNumber == element.race)
                        {
                            winRateVsRace = element.winrate;
                        }
                    });
                }

                if (element.map == map){
                    element.winLosses.forEach(element => {
                        if (playerTwoRaceNumber == element.race)
                        {
                            winRateVsRaceOnMap = element.winrate;
                        }
                    });
                }
            });
        }
    });

    if (winRateVsRaceOnMap == null) {
        winRaveVsRaceOnMap = "N/A"
    } else {
        winRaveVsRaceOnMap = " " + (winRateVsRaceOnMap * 100).toFixed(2) + "%";
    }

    if (winRateVsRace == null) {
        winRateVsRace = "N/A"
    } else {
        winRateVsRace = " " + (winRateVsRace * 100).toFixed(2) + "%";
    }

    if (isPlayerOne)
    {
        document.getElementById('playerOneVSRace').innerHTML = winRateVsRace
        document.getElementById('playerOneVSMapRace').innerHTML = winRaveVsRaceOnMap
    } else {
        document.getElementById('playerTwoVSRace').innerHTML = winRateVsRace
        document.getElementById('playerTwoVSMapRace').innerHTML = winRaveVsRaceOnMap
    }
}

function GetRankIcon(leagueOrder)
{
    return "/static/rankIcons/" + Ranks[leagueOrder] + ".png"
}

function GetRaceImageURL(race)
{
    return "/static/raceIcons/" + Races[race] + ".png"
}

function GetMapImageLink(map)
{
    return "/static/mapIcons/" + Maps[map] + ".jpg"

}